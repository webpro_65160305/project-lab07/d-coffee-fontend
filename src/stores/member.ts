import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'Mana LoveChad', tel: '0881234567' },
    { id: 2, name: 'Manee Haveheart', tel: '0887654321' }
  ])

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  function clear() {
    currentMember.value = null
  }

  const currentMember = ref<Member | null>()

  return {
    currentMember,
    searchMember,
    clear,
    members
  }
})
