import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import MaterialService from '@/service/material'
import type { Product } from '@/types/Stock'

export const useStockStore = defineStore('stock', () => {
  const products = ref<Product[]>([])
  const loadingStore = useLoadingStore()
  const initialProduct: Product = {
    name: '',
    price: 0,
    quantity: 0,
    value: 0,
    stock: false
  }
  const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initialProduct)))
  async function getMaterials() {
    loadingStore.doLoad()
    const res = await MaterialService.getMaterials()
    products.value = res.data
    loadingStore.finish()
  }
  async function getMaterial(id: number) {
    loadingStore.doLoad()
    const res = await MaterialService.getMaterial(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }
  async function saveMaterial() {
    loadingStore.doLoad()
    const material = editedProduct.value
    //add new
    if (!material.id) {
      
      const res = await MaterialService.addNewMaterial(material)
    } //update
    else {
      console.log(JSON.stringify(material))
      const res = await MaterialService.updateMaterial(material)
    }
    await getMaterials()
    loadingStore.finish()
  }
  async function deleteMaterial() {
    loadingStore.doLoad()
    const material = editedProduct.value
    const res = await MaterialService.removeMaterial(material)
    await getMaterials()
    loadingStore.finish()
  }
  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return {
    getMaterials,
    getMaterial,
    saveMaterial,
    deleteMaterial,
    products,
    editedProduct,
    clearForm
  }
})
