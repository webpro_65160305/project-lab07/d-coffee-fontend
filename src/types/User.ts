type Gender = 'Male' | 'Female' | 'Others'
type Role = 'Manager' | 'Employee'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender
  roles: Role
  logintime: string
  logouttime: string
  date: Date
}

export type { Gender, Role, User }
