type Product = {
  id?: number
  name: string
  price: number
  quantity: number
  value: number
  stock: boolean
}
export type { Product }
