type Gender = 'Male' | 'Female' | 'Others'
type Role = 'Manager' | 'Barista'

type User = {
  id: number
  name: string
  gender: Gender
  tel: string
  username: string
  password: string
  role: Role
  timein: Date | string
  timeout: Date | string
  timework: number
}

export const users: User[] = [
  {
    id: 1,
    name: 'Mark Poi',
    gender: 'Male',
    tel: '088-888-8888',
    username: 'mark',
    password: 'mark',
    role: 'Manager',
    timein: '',
    timeout: '',
    timework: 0
  },
  {
    id: 2,
    name: 'Arm Koi',
    gender: 'Male',
    tel: '088-888-8888',
    username: 'arm',
    password: 'arm',
    role: 'Barista',
    timein: '',
    timeout: '',
    timework: 0
  },
  {
    id: 3,
    name: 'Cake Mock',
    gender: 'Female',
    tel: '088-888-8888',
    username: 'cake',
    password: 'cake',
    role: 'Manager',
    timein: '',
    timeout: '',
    timework: 0
  },
  {
    id: 4,
    name: 'John Ney',
    gender: 'Male',
    tel: '088-888-8888',
    username: 'john',
    password: 'john',
    role: 'Barista',
    timein: '',
    timeout: '',
    timework: 0
  },
  {
    id: 5,
    name: 'Jane Jajo',
    gender: 'Female',
    tel: '088-888-8888',
    username: 'jane',
    password: 'jane',
    role: 'Barista',
    timein: '',
    timeout: '',
    timework: 0
  }
]

export { users as user }
export type { User, Gender, Role }
