import type { Product } from '@/types/Stock'
import http from './http'

function addNewMaterial(material: Product) {
  return http.post(`/materials`, material)
}

function updateMaterial(material: Product){
  return http.patch(`/materials/${material.id}`, material)
}
function removeMaterial(material: Product) {
  return http.delete(`/materials/${material.id}`)
}
function getMaterials() {
  return http.get('/materials')
}
function getMaterial(id: number) {
  return http.get(`/materials/${id}`)
}
export default { addNewMaterial, updateMaterial, removeMaterial, getMaterials, getMaterial }
